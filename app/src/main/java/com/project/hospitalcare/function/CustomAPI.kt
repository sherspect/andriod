package com.project.hospitalcare.function

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject

class CustomAPI {
    val baseURL : String = "https://api.hospitalcare.co.in/"
    val greet : String = "greet_user"
    val hb : String = "healthbuddy"
    var generalFunctions : GeneralFunction = GeneralFunction()

    /*
     int DEPRECATED_GET_OR_POST = -1;
        int GET = 0;
        int POST = 1;
        int PUT = 2;
        int DELETE = 3;
        int HEAD = 4;
        int OPTIONS = 5;
        int TRACE = 6;
        int PATCH = 7;
     */

    fun requestJSON(
        method : Int,
        context: Context,
        url: String,
        listenerResponse: Response.Listener<JSONObject?>,
        listenerError: Response.ErrorListener,
        apiBody: JSONObject
    )
    {

        generalFunctions.logCreate("url >>> $url")
        generalFunctions.logCreate("apiBody >>> $apiBody")

        val jsonRequest : JsonObjectRequest = JsonObjectRequest(
           // Request.Method.POST,
            method,
            url,
            apiBody,
            listenerResponse,
            listenerError
        )

        Volley.newRequestQueue(context).add(jsonRequest);
    }

    fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if (connectivityManager != null) {
                val capabilities =
                    connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
                if (capabilities != null) {
                    if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                        // Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                        return true
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                        //  Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                        return true
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                        //  Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                        return true
                    }
                }
            }
            return false
        }
        else
        {
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            return activeNetworkInfo != null && activeNetworkInfo.isConnected
        }
    }
}