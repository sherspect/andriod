package com.project.hospitalcare.function

import android.util.Log

class GeneralFunction {
    val debug : Boolean = true
    val logText : String = "Sanchyan"
    fun buildVersion(): Boolean {
        return debug;
    }
    fun logCreate(s: String) {
        if(debug) {
            Log.d(logText, s)
        }
    }
}