package com.project.hospitalcare

class Constant {
    val type : String = "type"
    val user : String = "user"
    val bot : String = "chatBot"
    val body : String = "body"
    val hb : String = "Healthbuddy"
    val atoz : String = "A-Z Health Symptoms"
    val sc : String = "Symptom Chekcer"
    val message : String = "message"
    val rsp : String = "response"
    val apiError : String = "API Error "
    val email : String = "Please Contact with Support at sanchyanchakraborty@gmail.com"
    val phone : String = "OR +91 9088601337"
    val noInternet : String = "Please Check Your Internet Connection"
    val greet : String = "I am HealthBuddy. Your personal healthcare companion for all your medical queries. "
}