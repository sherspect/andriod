package com.project.hospitalcare.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import com.project.hospitalcare.R
import com.project.hospitalcare.function.GeneralFunction



class MainActivity : AppCompatActivity() {
    var gf : GeneralFunction = GeneralFunction()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        gf.logCreate("Testing First Log in onCreate Main Activity >>>")
        navigate()
    }

    override fun onStart() {
        super.onStart()
        gf.logCreate("Testing First Log in onStart Main Activity >>>")
    }

    private fun navigate() {
        Handler().postDelayed({
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            finish()
        }, 2000)
    }
}