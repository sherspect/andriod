package com.project.hospitalcare.activity

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Response
import com.project.hospitalcare.Constant
import com.project.hospitalcare.R
import com.project.hospitalcare.adapter.CustomAdapter
import com.project.hospitalcare.function.CustomAPI
import com.project.hospitalcare.function.GeneralFunction
import com.project.hospitalcare.model.ItemsViewModel
import org.json.JSONObject


class ChatActivity : AppCompatActivity() {
    var constant : Constant = Constant()
    var gf : GeneralFunction = GeneralFunction()
    var selectedType : String = ""
    var chatText : String = ""
    var apiCustom : CustomAPI = CustomAPI()
    lateinit var chatRecyclerView : RecyclerView
    lateinit var textField : EditText
    lateinit var sendButton : ImageView
    lateinit var adapter : CustomAdapter
    val data = ArrayList<ItemsViewModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        chatRecyclerView = findViewById(R.id.chatRecyclerView)

        // this creates a vertical layout Manager
        chatRecyclerView.layoutManager = LinearLayoutManager(this)
        sendButton = findViewById(R.id.textSendButton)
        textField = findViewById(R.id.user_inputed_text)

        // ArrayList of class ItemsViewModel


        // This loop will create 20 Views containing
        // the image with the count of view
        /*
          val data = ArrayList<ItemsViewModel>()
        for (i in 1..50) {
            var jsonObject : JSONObject = JSONObject()
            val result = i % 2
            gf.logCreate("Checking raminder  >>>$result")
            if(result == 0)
            {
                jsonObject.put(constant.type,constant.user)
                jsonObject.put(constant.body, "Item " + i)
            }
            else
            {
                jsonObject.put(constant.type,constant.bot)
                jsonObject.put(constant.body, "Item " + i)
            }
            data.add(ItemsViewModel(jsonObject))
          //  data.add(ItemsViewModel(R.drawable.bot, "Item " + i))
        }

        // This will pass the ArrayList to our Adapter
        val adapter = CustomAdapter(data)

        // Setting the Adapter with the recyclerview
        chatRecyclerView.adapter = adapter
        chatRecyclerView.scrollToPosition(data.count()-1)
       // chatRecyclerView.scrollVerticallyTo(0);

         */
        setMicIcon()
        disabletextfield()
        chooserAlertDialog()
        sendButton.setOnClickListener()
        {
            gf.logCreate(" sendButton tapped >>> " )
            chatText = textField.text.toString()
            proceedChatAPICall()
        }
        textField.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                gf.logCreate(" afterTextChanged >>> " +s)
                if(s?.length ?:0  > 0)
                {
                    sendButton.setImageDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.send))

                }
                else
                {
                    setMicIcon()
                }

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                gf.logCreate(" beforeTextChanged >>> " +s)
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                gf.logCreate(" onTextChanged >>> " +s)
            }
        })


        apiCall(apiCustom.greet,0)

    }

    private fun proceedChatAPICall() {
        if(chatText.isNotEmpty())
        {
            if(selectedType == constant.hb)
            {
                // HalthBuddy API
                var jsonObject : JSONObject = JSONObject()
                jsonObject.put(constant.type,constant.user)
                jsonObject.put(constant.message, chatText)
                data.add(ItemsViewModel(jsonObject))
                adapter = CustomAdapter(data, applicationContext)
                chatRecyclerView.adapter = adapter
                chatRecyclerView.scrollToPosition(data.count()-1)
                textField.setText("")
                apiCall(apiCustom.hb, 1)

            }
            else  if(selectedType == constant.atoz)
            {
                // A to Z API
            }
            else  if(selectedType == constant.sc)
            {
                // Symptom Checker API
            }
        }
        else
        {
            // Voice Search
        }
    }

    private fun apiCall(greet: String, i: Int) {
        if(apiCustom.isOnline(applicationContext))
        {
            var apiBody : JSONObject = JSONObject()

            if(greet == apiCustom.greet) {
                apiCustom.requestJSON(
                    i, applicationContext, apiCustom.baseURL + apiCustom.greet, listenerResponse,
                    listenerError,
                    apiBody
                )
            }
            else if(greet == apiCustom.hb)
            {
                apiBody.put(constant.message, chatText)
                apiCustom.requestJSON(
                    i, applicationContext, apiCustom.baseURL + apiCustom.hb, listenerResponse2,
                    listenerError,
                    apiBody
                )
            }
        }
        else
        {
            var jsonObject : JSONObject = JSONObject()
            jsonObject.put(constant.type,constant.bot)
            var message : String = ""
            if(greet == apiCustom.greet)
            {
                message = constant.greet+"\n"+"\n"+constant.noInternet
            }
            else
            {
                message = constant.noInternet
            }

            jsonObject.put(constant.message, message)
            data.add(ItemsViewModel(jsonObject))
            adapter = CustomAdapter(data, applicationContext)
            chatRecyclerView.adapter = adapter
            chatRecyclerView.scrollToPosition(data.count()-1)

        }
    }

    private fun chooserAlertDialog() {
        val builder = AlertDialog.Builder(this)
        //set title for alert dialog
        builder.setTitle(R.string.dialogTitle)
        //set message for alert dialog
        builder.setMessage(R.string.dialogMessage)
        builder.setIcon((ContextCompat.getDrawable(applicationContext, R.drawable.bot)))

        //performing positive action
        builder.setPositiveButton(constant.atoz){dialogInterface, which ->
          //  Toast.makeText(applicationContext,"clicked yes",Toast.LENGTH_LONG).show()
            selectedType = constant.atoz
            enabletextfield()
        }
        //performing cancel action
        builder.setNeutralButton(constant.hb){dialogInterface , which ->
          //  Toast.makeText(applicationContext,"clicked cancel\n operation cancel",Toast.LENGTH_LONG).show()
            selectedType = constant.hb
            enabletextfield()
        }
        //performing negative action
        builder.setNegativeButton(constant.sc){dialogInterface, which ->
          //  Toast.makeText(applicationContext,"clicked No",Toast.LENGTH_LONG).show()
            selectedType = constant.sc
            enabletextfield()
        }
        // Create the AlertDialog
        val alertDialog: AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    private fun enabletextfield() {
        textField.isEnabled = true
    }

    private fun disabletextfield() {
        textField.isEnabled = false
    }

    private fun setMicIcon() {
        sendButton.setImageDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.mic))
    }

    override fun onResume() {
        super.onResume()


    }
    private val listenerResponse2: Response.Listener<JSONObject?> =
        Response.Listener<JSONObject?> { arg0 -> // Do what you want with response
            if(gf.buildVersion()) {
                gf.logCreate(" onResponse >>> " + JSONObject(arg0.toString()))
            }
            var jsonObject : JSONObject = JSONObject()
            jsonObject.put(constant.type,constant.bot)
            jsonObject.put(constant.message, JSONObject(arg0.toString()).get(constant.rsp))
            data.add(ItemsViewModel(jsonObject))
            val adapter = CustomAdapter(data, applicationContext)
            chatRecyclerView.adapter = adapter
            chatRecyclerView.scrollToPosition(data.count()-1)

        }
    private val listenerResponse: Response.Listener<JSONObject?> =
        Response.Listener<JSONObject?> { arg0 -> // Do what you want with response
            if(gf.buildVersion()) {
                gf.logCreate(" onResponse >>> " + JSONObject(arg0.toString()))
            }
            var jsonObject : JSONObject = JSONObject()
            jsonObject.put(constant.type,constant.bot)
            jsonObject.put(constant.message, JSONObject(arg0.toString()).get(constant.message))
            data.add(ItemsViewModel(jsonObject))
            val adapter = CustomAdapter(data, applicationContext)
            chatRecyclerView.adapter = adapter
            chatRecyclerView.scrollToPosition(data.count()-1)

        }
    private val listenerError: Response.ErrorListener =
        Response.ErrorListener { arg0 ->
            if(gf.buildVersion()) {
                gf.logCreate(" onErrorResponse $arg0")

            }
            var jsonObject : JSONObject = JSONObject()
            jsonObject.put(constant.type,constant.bot)
            jsonObject.put(constant.message, constant.apiError + arg0+"\n"+"\n"+constant.email+"\n"+constant.phone)
            data.add(ItemsViewModel(jsonObject))
            val adapter = CustomAdapter(data, applicationContext)
            chatRecyclerView.adapter = adapter
            chatRecyclerView.scrollToPosition(data.count()-1)
        }


}