package com.project.hospitalcare.adapter

import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.marginLeft
import androidx.core.view.marginRight
import androidx.recyclerview.widget.RecyclerView
import com.project.hospitalcare.Constant
import com.project.hospitalcare.R
import com.project.hospitalcare.function.GeneralFunction
import com.project.hospitalcare.model.ItemsViewModel
import org.json.JSONObject

class CustomAdapter(private val mList: List<ItemsViewModel>, applicationContext: Context) : RecyclerView.Adapter<CustomAdapter.ViewHolder>() {
    var gf : GeneralFunction = GeneralFunction()
    var constant : Constant = Constant()
    var context : Context = applicationContext
    override fun getItemViewType(position: Int): Int {
        val ItemsViewModel = mList[position]
        gf.logCreate("Checking getItemViewType ItemsViewModel >>>${ItemsViewModel.jsonObject}")
        val jsonObject : JSONObject = ItemsViewModel.jsonObject
        var count : Int = 0
        if(jsonObject.get(constant.type) == constant.bot)
        {
            count = 0
        }
        else if(jsonObject.get(constant.type) == constant.user)
        {
            count = 1
        }
        // 0 for bot, 1 for user
        return count
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        gf.logCreate("Checking onCreateViewHolder viewType >>>$viewType")
        var view : View = LayoutInflater.from(parent.context)
            .inflate(R.layout.card_view_design, parent, false)

        if(viewType == 0) {
            view = LayoutInflater.from(parent.context)
                .inflate(R.layout.card_view_design, parent, false)

        }
        else if(viewType == 1)
        {
            view = LayoutInflater.from(parent.context)
                .inflate(R.layout.card_view_design_for_user, parent, false)
        }
        return ViewHolder(view)

    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        gf.logCreate("Checking onBindViewHolder position >>>$position")
        val ItemsViewModel = mList[position]
        /*
        if(position == 2)
        {

            holder.textView.gravity = Gravity.RIGHT
            (holder.textView.layoutParams as LinearLayout.LayoutParams).apply {
                // individually set text view any side margin
                marginStart = 25
                topMargin = 35
                // or set margins in this way
                /*setMargins(
                    25.dpToPixels(context), // left/start margin
                    35.dpToPixels(context), // top margin
                    50.dpToPixels(context), // right/end margin
                    0 // bottom margin
                )*/
            }


        }

         */
        // sets the text to the textview from our itemHolder class
        gf.logCreate("Checking onBindViewHolder ItemsViewModel >>>${ItemsViewModel.jsonObject}")
        holder.textView.text = ItemsViewModel.jsonObject.getString(constant.message)
        if(ItemsViewModel.jsonObject.getString(constant.type) == constant.bot)
        {
            holder.imageView.setImageDrawable(ContextCompat.getDrawable(context,(R.drawable.bot)))
        }

    }

    override fun getItemCount(): Int {
        gf.logCreate("Checking getItemCount mList.size >>>"+mList.size)
        return mList.size
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val imageView: ImageView = itemView.findViewById(R.id.imageview)
        val textView: TextView = itemView.findViewById(R.id.textView)
    }
}
